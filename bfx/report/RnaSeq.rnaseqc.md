### Trimming and Alignment Metrics per Sample

General summary statistics are provided per sample. Sample readsets are merged for clarity.

Table: Trimming and Alignment Statistics per Sample (**partial table**; [download full table](trimAlignmentTable.tsv))

$trim_alignment_table$

* Raw Reads: total number of reads obtained from the sequencer
* Surviving Reads: number of remaining reads after the trimming step
* %: Surviving Reads / Raw Reads
* Aligned Reads: number of aligned reads to the reference
* %: Aligned reads / Surviving reads
* Alternative Alignments: number of duplicate read entries providing alternative coordinates
* %: Alternative Alignments / Aligned Reads
* rRNA Reads: number of reads aligning to rRNA regions as defined in the transcript model definition
* %: rRNA Reads / Surviving Reads
* Coverage:Median Exon CV. The median coefficient of variation of exon coverage. Exon coverage is computed by dropping the first and last 500bp of each gene and measuring the "High Quality" (above) coverage over the remainder of the exons. This is considered a good metric for sample quality. A lower value indicates more consistent coverage over exons
* Exonic Rate: fraction mapping reads within exons
* Genes: number of Genes with at least 5 reads

[Additional metrics can be found in the original RNAseqQC report available here](reportRNAseqQC.zip)
